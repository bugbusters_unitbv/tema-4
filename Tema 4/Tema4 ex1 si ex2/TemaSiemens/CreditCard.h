#pragma once

#include<iostream>
#include"Money.h"
#include<map>
#include<string.h>
using namespace std;

class CreditCard
{
	private:
		string ownerName;
		string cardNumber;
		map<string, Money>transactions;
	public:
		CreditCard();
		~CreditCard();
		CreditCard(string,string);
		void printAllTransactions();
		void chargeMoney(string, Money);
		void chargeMoney2(string, int, int);

};
