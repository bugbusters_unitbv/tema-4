#include<iostream>
using namespace std;


class Money
{
private:
	int euros;
	int centimes;

public:
	Money();
	Money(int, int);
	int getEuros();
	int getCentimes();
	void setEuros(int);
	void setCentimes(int);
	void printInfo();
	friend istream& operator >>(istream& , Money&);
	friend ostream& operator <<(ostream& , Money);
	Money operator +(Money&);
	Money operator -(Money&);
	Money operator /(Money&);
	Money operator *(Money&);
	bool operator !=(Money&);
	bool operator ==(Money&);

};
