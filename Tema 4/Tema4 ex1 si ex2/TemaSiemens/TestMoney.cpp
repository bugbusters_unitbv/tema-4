#include<iostream>
#include"CreditCard.h"
#include<string>

using namespace std;

void main()
{
	try
	{
		Money money1(85, 36);
		Money money2;
		int switch_on;
		bool exitCondition = false;
		money2.setCentimes(936);
		cout << "Alegeti optiunea:" << endl << "1)Operatii cu Money" << endl << "2)Operatii cu Credit card" << endl << "3)Exit" << endl;
		cin >> switch_on;

		Money money3 = money1 - money2;
		Money money4 = money1 + money2;

		CreditCard credit("Mihai", "0986");

		while (exitCondition != true)
		{
			switch (switch_on)
			{
			case 1:
				cout << "Money 1: " << money1 << endl;
				cout << "Money 2: " << money2 << endl;
				(money1 + money1 + money1).printInfo();
				if (money1 != money2)
					cout << "Valori diferite." << endl;
				cout << "Money2 - Money1: " << money3 << endl;

				money2.setEuros(500);
				cout << "Money1 + Money2: " << money4;
				cout << (money1 == money2) << endl;

				cout << "Alegeti alta optiune:";
				cin >> switch_on;
				cout << endl;
				break;

			case 2:
				credit.chargeMoney("Paine", money1);
				credit.chargeMoney("Carne", money2);
				credit.chargeMoney("Televizor", money4);
				credit.chargeMoney("Telefon", money3);
				credit.chargeMoney2("Lapte", 10, 1675);
				credit.printAllTransactions();

				cout << "Alegeti alta optiune:";
				cin >> switch_on;
				cout << endl;
				break;

			case 3:
				exitCondition = true;

			default:
				break;
			}
		}
		
	}
	catch (const char* msg)
	{
		cerr << msg << endl;
	}

}

