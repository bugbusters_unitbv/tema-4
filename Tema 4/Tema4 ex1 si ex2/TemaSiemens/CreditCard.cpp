#include<iostream>
#include "CreditCard.h"
#include <string>

CreditCard::CreditCard()
{
	this->cardNumber = "";
	this->ownerName = "";
}

CreditCard::~CreditCard()
{

}

CreditCard::CreditCard(string ownerName, string cardNumber)
{
	this->ownerName = ownerName;
	this->cardNumber = cardNumber;
}

void CreditCard::printAllTransactions()
{
	for (const auto& kv : transactions) 
	{
		cout << kv.first << ": " << kv.second << std::endl;
	}
	/*typedef map<string, Money>::const_iterator MapIterator;
	for (MapIterator iter = transactions.begin(); iter != transactions.end(); iter++)
	{
		cout << iter->first << endl;
		cout << "Values:" << endl;
	}*/
}

void CreditCard::chargeMoney(string name, Money cost)
{
	transactions.insert(pair<string, Money>(name, cost));
}

void CreditCard::chargeMoney2(string name, int euro, int centimes)
{
	Money money(euro, centimes);
	transactions.insert(pair<string, Money>(name, money));
}
