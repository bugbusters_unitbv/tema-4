#include "Money.h"
#include<iostream>

using namespace std;

Money::Money()
{
	this->centimes = 0;
	this->euros = 0;
	
}

Money::Money(int euros, int centimes)
{
	
	if (centimes >= 100)
	{
		euros+=centimes/100;
		centimes = centimes % 100;
	}
	this->centimes = centimes;
	this->euros = euros;
}

int Money::getEuros()
{
	return this->euros;
}

int Money::getCentimes()
{
	
	return this->centimes;
}

void Money::setEuros(int euros)
{
	this->euros = euros;

}

void Money::setCentimes(int centimes)
{
	
	if (centimes >= 100)
	{
		this->euros+=centimes/100;
		centimes = centimes % 100;
	}
	this->centimes = centimes;
}

void Money::printInfo()
{
	cout << "Euros= " << this->euros<<endl;
	cout << "Centimes= " << this->centimes<<endl;
}



istream& operator >> (istream&flux, Money &m)
{
	cout << "Give amount of euros:= ";
	flux >> m.euros;
	cout << "Give amount of cenimes:= ";
	flux >> m.centimes;
	return flux;
}

ostream& operator<<(ostream&flux, Money m)
{
	flux << "Number of euros:= " << m.euros<<endl;
	flux << "Number of centimes:= " << m.centimes<<endl;
	return flux;
}

Money Money::operator+(Money &a)
{
	Money money;
	money.euros = this->euros + a.euros;
	money.centimes = this->centimes + a.centimes;
	if (money.centimes >= 100)
	{
		money.euros++;
		money.centimes -= 100;
	}
	return money;
}

Money Money::operator-(Money &m)
{
	Money money;
	money.euros = this->euros - m.euros;
	money.centimes = this->centimes - m.centimes;
	if (money.centimes < 0)
	{
		money.euros--;
		money.centimes += 100;
	}
	else if (money.euros < 0)
	{
		throw"Number of euros is negative";
	}
	return money;
}

Money Money::operator/(Money &m)
{
	Money money;
	float zamfir=m.euros+(float)m.centimes/100;
	float eliza = this->euros + (float)this->centimes / 100;
	float zameliza = eliza/ zamfir;
	money.euros = floor(zameliza);
	money.centimes = (zameliza - floor(zameliza)) * 100;
	
	return money;

}

Money Money::operator*(Money &m)
{
	Money money;
	float zamfir = m.euros + (float)m.centimes / 100;
	float eliza = this->euros + (float)this->centimes / 100;
	float zameliza = eliza * zamfir;
	money.euros = floor(zameliza);
	money.centimes = (zameliza - floor(zameliza)) * 100;
	return money;
}

bool Money::operator!=(Money &m)
{
	if (m.euros == this->euros && m.centimes == this->euros)
		return false;
	return true;
}

bool Money::operator==(Money &m)
{
	if (m.euros == this->euros && m.centimes == this->euros)
		return true;
	return false;
}