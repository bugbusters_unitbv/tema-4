#pragma once
#include <iostream>
#include <string>

using namespace std;
class Customer {

private:
	string name;
	bool member;
	string memberType;

public:
	Customer(string, bool, string);
	Customer();
	~Customer();
	string getName();
	bool isMember();
	void setMember(bool);
	string getMemberType();
	void setMemberType(string);
	string toString();

};