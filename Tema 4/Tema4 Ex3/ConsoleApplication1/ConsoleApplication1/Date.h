#pragma once
class Date
{
private:
	int month;
	int day;
	int year;

public:
	Date();
	Date(int month, int day, int year);
	string display1();
	void display2();
	void increment();
};