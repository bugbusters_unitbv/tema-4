#include "Discount.h"

double Discount::getServiceDiscountRate(string memberType)
{
	if(memberType.compare("Premium") == 0)
		return this->serviceDiscountPremium;
	else if (memberType.compare("Gold") == 0)
		return this->serviceDiscountGold;
	else if (memberType.compare("Silver") == 0)
		return this->serviceDiscountSilver;
}

double Discount::getProductDiscountRate(string memberType)
{
	if (memberType.compare("Premium") == 0)
		return this->productDiscountPremium;
	else if (memberType.compare("Gold") == 0)
		return this->productDiscountGold;
	else if (memberType.compare("Silver") == 0)
		return this->productDiscountSilver;
}
