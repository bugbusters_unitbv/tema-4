#pragma once
#include <iostream>
#include "Customer.h"
#include "Date.h"
#include <ctime>

using namespace std;

class Visit {

private:
	Customer customer;
	time_t date;
	double serviceExpense;
	double productExpense;

public:
	Visit(Customer, time_t, double, double);
	Visit();
	~Visit();
	string getName();
	double getServiceExpense();
	void setServiceExpense(double);
	double getProductExpense();
	void setProductExpense(double);
	double getTotalExpense();
	string toString();
	string getMembership();

};