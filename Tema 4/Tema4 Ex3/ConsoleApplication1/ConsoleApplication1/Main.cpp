#include <iostream>
#include "Visit.h"
#include "Discount.h"
#include <vector>
#define _CRT_SECURE_NO_WARNINGS

using namespace std;
void applyExpences(Visit visit)
{
	Discount d;
	if (visit.getMembership().compare("NAM")!=0)
	{
		visit.setServiceExpense(visit.getServiceExpense() - visit.getServiceExpense()*d.getServiceDiscountRate(visit.getMembership()));
		visit.setProductExpense(visit.getProductExpense() - visit.getProductExpense()*d.getProductDiscountRate(visit.getMembership()));
	}
	cout << visit.toString() << endl;
	cout << visit.getTotalExpense()<<endl;
}

void main()
{
	vector<Customer> clientList;
	clientList.reserve(4);
	Customer customer("Mihai", true, "Gold");
	Customer customer1("Alex", true, "Premium");
	Customer customer2("Bogdan", true, "Silver");
	Customer customer3("Adi", false, "NAM");
	clientList.push_back(customer);
	clientList.push_back(customer1);
	clientList.push_back(customer2);
	clientList.push_back(customer3);

	time_t now = time(0);
	
	vector<Visit> visitList;

	for (int i = 0; i < 9; i++)
	{
		int randClient = rand() % 4 + 0;
		double randServiceExpense = rand() % 1000 + 100;
		double randProductExpense = rand() % 1000 + 100;
		Visit visit(clientList.at(randClient), now, randServiceExpense, randProductExpense);
		visitList.push_back(visit);
	}

	for (int i = 0; i < visitList.size(); i++)
	{
		applyExpences(visitList.at(i));
	}


}