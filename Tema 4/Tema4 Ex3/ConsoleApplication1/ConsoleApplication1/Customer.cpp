#include "Customer.h"

Customer::Customer(string name, bool member, string memberType)
{
	this->name = name;
	this->member = member;
	this->memberType = memberType;
}

Customer::Customer()
{

}

Customer::~Customer()
{

}

string Customer::getName()
{
	return this->name;
}

bool Customer::isMember()
{
	if(this->member == true)
		return true;
	return false;
}

void Customer::setMember(bool b)
{
	this->member = b;
}

string Customer::getMemberType()
{
	return this->memberType;
}

void Customer::setMemberType(string memberType)
{
	this->memberType = memberType;
}

string Customer::toString()
{
	string rez = this->name + " ";
	if (this->member == true)
		rez += this->memberType;
	else
		rez += "Not a member";
	return rez;
}
